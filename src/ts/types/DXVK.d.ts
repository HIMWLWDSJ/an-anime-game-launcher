type DXVK = {
    title: string;
    version: string;
    uri: string;
    recommended: boolean;
    installed: boolean;
};

export type { DXVK };
