# Usage statistics

This file is a launcher usage statistics archive. You can see here which journey we made to make the launcher looks like it looks

Our current statistics you can find in [readme](../../README.md)

> You can suggest colors for your countries

## 2.6.0

| Period | Source | Data |
| - | - | - |
| 10 Mar — ? | Discord server | <img src="../pics/stats/2022/mar-apr.png" height="400px"> |

## 2.5.0

| Period | Source | Data |
| - | - | - |
| 20 Feb — 10 Mar | Discord server | <img src="../pics/stats/2022/feb-mar.png" height="400px"> |
| 10 Mar — ? | Discord server | <img src="../pics/stats/2022/mar-apr.png" height="400px"> |

### In-launcher analytics

> Note that this statistics was gathered only from newcomers, so it actually means 154 new users of the launcher

<img src="../pics/stats/2.5.0.png">

## 2.3.0 — 99 total — in-launcher analytics

<img src="../pics/stats/2.3.0.png">

## 2.2.0 — 29 total — in-launcher analytics

<img src="../pics/stats/2.2.0.png">
